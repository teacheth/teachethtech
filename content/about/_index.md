---
title: "About TeachEth.Tech"
date: 2019-06-06T12:39:49+12:00
draft: false
---


We are trying to organize an unconference (that's a fancy way of saying a gathering, or a conference with a strong focus on interaction, conversation, and horizontal knowledge exchange) for everybody teaching (or otherwise active in education) ethics in data science and near fields.

It's a grassroot initiative (read: at the moment, nobody is putting money to sustain it, and we can do what we believe it's better for us all).

You can contribute by joining the conversation on Keybase: search for the [teacheth team](https://keybase.io/team/teacheth).

This website is written in Markdown using Hugo, deployed by Netlify, and lives on an open Gitlab [repository](https://gitlab.com/teacheth/teachethtech). You can help improve it by commenting or editing it.
